#!/usr/bin/env python3

from os import getenv, listdir, path
from sys import argv, stdout
from typing import Iterator, Any

HOME = getenv('HOME')

stop_at_home_boundary = True
stop_at_device_boundary = True
device_boundary_is_target = True

target_dirs = set((
    '.git',
))

target_files = set((
    'go.mod',
    'makefile',
    'readme',
    'readme.md',
    'readme.rst',
    'readme.txt',
    'requirements.txt',
    'setup.py',
))


def expand(path_: str) -> str:
    return path.abspath(path.expanduser(path_))


def climb(starting_point: str) -> Iterator:
    '''The opposite of os.walk()'''
    def list_items(directory: str) -> tuple:
        items = listdir(directory)
        return (
            directory,
            tuple(d for d in items if path.isdir(path.join(directory, d))),
            tuple(f for f in items if path.isfile(path.join(directory, f))),
        )

    last = cur = expand(starting_point)
    yield list_items(cur)

    while True:
        cur = path.split(cur)[0]

        if cur == last:
            break

        last = cur
        yield list_items(cur)


def find_root(start: str) -> Any:
    start = expand(start)

    if path.isfile(start):
        start = path.dirname(start)

    for curdir, dirs, files in climb(start):
        if not curdir.startswith(HOME) and stop_at_home_boundary:
            return start

        if any(d.lower() in target_dirs for d in dirs):
            return curdir

        if any(f.lower() in target_files for f in files):
            return curdir

        if path.ismount(curdir):
            if device_boundary_is_target:
                return curdir

            if stop_at_device_boundary:
                return start

    return start


if __name__ == '__main__':
    stdout.write(find_root(argv[1]))
