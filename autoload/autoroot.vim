" ==========================================================================
" Try to find the project's root directory relative to the current file.
" ==========================================================================

let s:plug_path = fnamemodify(resolve(expand('<sfile>:p')), ':h:h')
let s:autoroot = s:plug_path.'/bin/autoroot.py'

function! autoroot#AutoRoot(start) abort
  if !executable(s:autoroot)
    echoerr 'autoroot.vim: could not find autoroot.py'
    return
  endif

  if !exists('b:autoroot_dir')
    let b:autoroot_dir = system(printf('%s %s', s:autoroot, a:start))
  endif

  if b:autoroot_dir ==# getcwd()
    return
  endif

  try
    call chdir(b:autoroot_dir)
  catch /E472/
  endtry
endfunction
