if exists('g:autoroot_loaded')
  finish
endif
let g:autoroot_loaded = 1

if exists('+autochdir') && &autochdir
  set noautochdir
endif

augroup autoroot
  autocmd!
  autocmd BufEnter * call autoroot#AutoRoot(expand('%:p:h'))
augroup END
